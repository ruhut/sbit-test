#variable "AWS_REGION" {
#	default = "ap-southeast-1"
#}

variable "region" {}
variable "main_vpc_cidr" {}
variable "public_subnets" {}
variable "private_subnets" {}
variable "ec2_ami" {}
variable "ec2_type" {}
variable "ec2_key" {}