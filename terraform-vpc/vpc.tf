resource "aws_vpc" "sbit-vpc" {
	cidr_block = var.main_vpc_cidr
	enable_dns_support = true
	enable_dns_hostnames = true
	enable_classiclink = false
	instance_tenancy = "default"

	tags = {
		Name = "sbit-vpc"
	}
}

resource "aws_internet_gateway" "sbit-igw" {
	vpc_id =  aws_vpc.sbit-vpc.id
	tags = {
		Name = "sbit-igw"
	}
}

resource "aws_subnet" "sbit-public-subnet" {
	vpc_id = aws_vpc.sbit-vpc.id
	cidr_block = "${var.public_subnets}"
	
	tags = {
		Name = "sbit-public-subnet"
	}
}

resource "aws_subnet" "sbit-private-subnet" {
	vpc_id = aws_vpc.sbit-vpc.id
	cidr_block = "${var.private_subnets}"
	
	tags = {
		Name = "sbit-private-subnet"
	}
}

resource "aws_eip" "natgw-ip" {
	vpc = true
	
	tags = {
		Name = "natgw-ip"
	}
}

resource "aws_nat_gateway" "sbit-natgw" {
	allocation_id = aws_eip.natgw-ip.id
	subnet_id = aws_subnet.sbit-public-subnet.id
	
	tags = {
		Name = "sbit-natgw"
	}
}

resource "aws_route_table" "sbit-public-rt" {
	vpc_id = aws_vpc.sbit-vpc.id

	route {
		cidr_block = "0.0.0.0/0"
		gateway_id = aws_internet_gateway.sbit-igw.id
	}
	
	tags = {
		Name = "sbit-public-rt"
	}
}

resource "aws_route_table" "sbit-private-rt" {
	vpc_id = aws_vpc.sbit-vpc.id

	route {
		cidr_block = "0.0.0.0/0"
		nat_gateway_id = aws_nat_gateway.sbit-natgw.id
	}
	
	tags = {
		Name = "sbit-private-rt"
	}
}

resource "aws_route_table_association" "sbit-public-rtassoc" {
	subnet_id = aws_subnet.sbit-public-subnet.id
	route_table_id = aws_route_table.sbit-public-rt.id
}

resource "aws_route_table_association" "sbit-private-rtassoc" {
	subnet_id = aws_subnet.sbit-private-subnet.id
	route_table_id = aws_route_table.sbit-private-rt.id
}