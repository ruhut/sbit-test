# Sbit Test

Hello, Stockbit!
This is my simple explanation about my script in this repo.

[1] Terraform HCL
Point a, b, c is configured in terraform-vpc/vpc.tf
Point d is configured in terraform-vpc/asg.tf

[2] CI/CD Process
All configured in .gitlab-ci.yml
- Build the Dockerfile
- Push to Gitlab Container Registry
- Copy deployment files to the server
- Pull latest image from Gitlab Container Registry
- Run with compose

In nginx vhost file (nginx-files/default.conf), the index file for nginx remain index.htm index.html
So, when we access the URL, it will returned "Welcome to nginx!" (default nginx welcome page)
You can access the txt file with "http://<URL>/hello.txt"

However, if you consider to get the content of hello.txt as index, you can uncomment line 11 and comment line 10 in nginx-files/default.conf
It makes "http://<URL>" returned the content of "hello.txt"

The EC2 IP Address (configured in .gitlab-ci.yml) should be unaccessible, because I might already terminated the EC2 or the Public IP has been changed

Thank you!
