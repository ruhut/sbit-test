resource "aws_launch_configuration" "sbit-lc" {
	name_prefix 	= "sbit-"
	
	image_id		= var.ec2_ami
	instance_type	= var.ec2_type
	key_name		= var.ec2_key
	
	security_groups = [ aws_security_group.allow-http.id ]
	associate_public_ip_address = false
	
	lifecycle {
		create_before_destroy = true
	}
}

resource "aws_elb" "sbit-elb" {
	name = "sbit-elb"
	security_groups = [
		aws_security_group.allow-http.id
	]
	subnets = [
		aws_subnet.sbit-public-subnet.id
	]
	
	health_check {
		healthy_threshold = 2
		unhealthy_threshold = 2
		timeout = 3
		interval = 30
		target = "HTTP:80/"
	}
	
	listener {
		lb_port = 80
		lb_protocol = "http"
		instance_port = "80"
		instance_protocol = "http"
	}
}

resource "aws_autoscaling_group" "sbit-asg" {
	name			 = "sbit-asg"
	max_size		 = 5
	min_size		 = 2
	desired_capacity = 2

	health_check_grace_period = 300
	health_check_type		  = "ELB"
	launch_configuration	  = aws_launch_configuration.sbit-lc.name
	load_balancers	= [
		aws_elb.sbit-elb.id
	]
	
	vpc_zone_identifier = [
		aws_subnet.sbit-private-subnet.id
	]
	
	lifecycle {
		create_before_destroy = true
	}
	
	tag {
		key					= "Name"
		value				= "sbit-asg"
		propagate_at_launch = true
	}
}

resource "aws_autoscaling_policy" "sbit-asg-policy-up" {
	name = "sbit-asg-policy-up"

	scaling_adjustment = 1
	adjustment_type    = "ChangeInCapacity"
	cooldown		   = 300

	autoscaling_group_name = aws_autoscaling_group.sbit-asg.name
}

resource "aws_cloudwatch_metric_alarm" "sbit-asg-cpu-alarm-up" {
	alarm_name	= "sbit-asg-cpu-alarm-up"
	comparison_operator	= "GreaterThanOrEqualToThreshold"
	evaluation_periods	= 2
	metric_name			= "CPUUtilization"
	namespace			= "AWS/EC2"
	period				= 120
	statistic			= "Average"
	threshold			= 45
	
	dimensions = {
		AutoScalingGroupName = aws_autoscaling_group.sbit-asg.name
	}
	
	alarm_description = "Monitor EC2 CPU Utilization > 45%"
	alarm_actions = [
		aws_autoscaling_policy.sbit-asg-policy-up.arn
	]
}

resource "aws_autoscaling_policy" "sbit-asg-policy-down" {
	name = "sbit-asg-policy-down"

	scaling_adjustment = 1
	adjustment_type    = "ChangeInCapacity"
	cooldown		   = 300

	autoscaling_group_name = aws_autoscaling_group.sbit-asg.name
}

resource "aws_cloudwatch_metric_alarm" "sbit-asg-cpu-alarm-down" {
	alarm_name	= "sbit-asg-cpu-alarm-down"
	comparison_operator	= "LessThanOrEqualToThreshold"
	evaluation_periods	= 2
	metric_name			= "CPUUtilization"
	namespace			= "AWS/EC2"
	period				= 120
	statistic			= "Average"
	threshold			= 45
	
	dimensions = {
		AutoScalingGroupName = aws_autoscaling_group.sbit-asg.name
	}
	
	alarm_description = "Monitor EC2 CPU Utilization < 45%"
	alarm_actions = [
		aws_autoscaling_policy.sbit-asg-policy-down.arn
	]
}

resource "aws_security_group" "allow-http" {
	name		= "allow-http"
	description	= "allow http inbound"
	vpc_id		= aws_vpc.sbit-vpc.id
	
	ingress {
		from_port	= 80
		to_port		= 80
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	
	egress {
		from_port	= 0
		to_port		= 0
		protocol	= -1
		cidr_blocks	= ["0.0.0.0/0"]
	}
	
	tags = {
		Name = "sbit-allow-http"
	}
}