FROM nginx:latest

WORKDIR /var/www

COPY nginx-files/default.conf /etc/nginx/conf.d/default.conf
COPY nginx-files/hello.txt /var/www/hello.txt

RUN ln -sf /usr/share/nginx/html/* . && \
    chmod 644 hello.txt
